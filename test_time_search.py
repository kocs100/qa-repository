from selenium.webdriver.chrome.webdriver import WebDriver
import allure


@allure.title('Тест поиска')
def test_time_search():
    driver = WebDriver(executable_path="C:\\Selenium\\chromedriver.exe")
    with allure.step('Открываем страницу Мэйла'):
        driver.get("https://mail.ru/")

    with allure.step('Ищем результат'):
        search_input = driver.find_element_by_xpath('//input[@id="q"]')
        search_input.send_keys('Как стать автотестировщиком за 3 дня')
        search_button = driver.find_element_by_id("search:submit")
        search_button.click()

    with allure.step('Сравниваем кол-во выданных результатов на странице с заданным'):
        search_results = driver.find_elements_by_xpath('//li[@class="result__li App-result"]')
        assert len(search_results) == 11
